const jslist = {
  headerCol1: "Nom",
  headerCol2: "Raccourcis",
  rows: [
    ["Consolelog", "cl | con"],
    ["Array function", "fn | fun"],
    ["Map function", "ma | map"],
    ["Filter function", "fi | fil"],
  ],
};
module.exports = jslist;
