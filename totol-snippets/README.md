# Totol Snippets 

## Description
A list of snippet I'm using in several languages

## Commands
All Commands are prefixed by "\[Totol\]"
- Get the list of snippets
``` CTRL + P > Type "Liste de tous les raccourcis" ```
or just use the shortcut
``` CTRL + ALT + E ```


## Languages List

### Javascript
| Name                    |      Shortcut      |
|-------------------------|:------------------:|
| console.log             | "cl", "con"        |
| Arrow function          | "fn", "fun"        |
| Map function            | "ma", "map"        |
| Filter function         | "fi", "fil"        |

### React Native
| Name                    |      Shortcut      |
|-------------------------|:------------------:|
| Functionnal component   | "rnfc", "imp"      |
| &ltView&gt&lt/View&gt   | "&ltTe", "Text"    |
| &ltText&gt&lt/Text&gt   | "&ltVi", "View"    |

### React
| Name                    |      Shortcut      |
|-------------------------|:------------------:|
| useEffect               | "use", "eff"       |
| useState                | "use", "sta"       |
| Functional component    | "rfc", "imp"       |