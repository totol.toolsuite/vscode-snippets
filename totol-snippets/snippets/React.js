const reactlist = {
  headerCol1: "Nom",
  headerCol2: "Raccourcis",
  rows: [
    ["useEffect", "use | eff"],
    ["useState", "use | sta"],
    ["React Functional Component", "rfc | imp"],
  ],
};

module.exports = reactlist;
