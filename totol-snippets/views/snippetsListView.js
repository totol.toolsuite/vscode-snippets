const buildTable = require("../snippets/Functions");
const jslist = require("../snippets/Javascript");
const reactlist = require("../snippets/React");
const rnlist = require("../snippets/ReactNative");

const snippetListView = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-top: 15px;
        margin-bottom: 15px;
      }

      h2 {
        font-size: 30px;
      }

      h3 {
        font-size: 25px;
      }

      th {
        font-size: 22px;
        font-weight: bold;
      }

      td, th {
        border: 1px solid #525050;
        text-align: left;
        padding: 8px;
        font-size: 20px
      }

      tr:nth-child(even) {
        background-color: #525050;
      }
    </style>
    <title>Raccourcis</title>
</head>
<body>
<h2>Liste des raccourcis</h2>
<h3>Javascript</h3>
${buildTable(jslist)}
<hr>
<h3>React Native</h3>
${buildTable(rnlist)}
<hr>
<h3>React</h3>
${buildTable(reactlist)}
</body>
</html>`;

module.exports = snippetListView;
