// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const snippetListView = require("./views/snippetsListView");

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log(
    'Congratulations, your extension "Totol Snippet" is now active! Eclate toi'
  );

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with  registerCommand
  // The commandId parameter must match the command field in package.json
  let totolCmd = vscode.commands.registerCommand("totol.totol", function () {
    vscode.window.showInformationMessage("Anatole est très gentil");
  });

  let cavaCmd = vscode.commands.registerCommand("totol.cava", async () => {
    const answer = await vscode.window.showInformationMessage(
      "Ca va ou quoi ?",
      "Yes",
      "Bof"
    );
    if (answer == "Yes") {
      vscode.window.showInformationMessage("Lourd poto!");
    } else {
      vscode.window.showInformationMessage("Ha ... cheh");
    }
  });

  context.subscriptions.push(totolCmd);
  context.subscriptions.push(cavaCmd);
  context.subscriptions.push(
    vscode.commands.registerCommand("totol.list", () => {
      // Create and show a new webview
      const panel = vscode.window.createWebviewPanel(
        "totol", // Identifies the type of the webview. Used internally
        "[TOTOL] Raccourcis", // Title of the panel displayed to the user
        vscode.ViewColumn.One, // Editor column to show the new webview panel in.
        {} // Webview options. More on these later.
      );
      panel.webview.html = snippetListView;
    })
  );
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
  activate,
  deactivate,
};
