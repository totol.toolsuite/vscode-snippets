const buildTable = (items) => {
  let htmlBloc = `
  <table>
    <tr>
      <th>${items.headerCol1}</th>
      <th>${items.headerCol2}</th>
    </tr>`;
  for (let row of items.rows) {
    htmlBloc += `
    <tr>
      <td>${row[0]}</td>
      <td>${row[1]}</td>
    </tr>`;
  }
  htmlBloc += "</table>";
  return htmlBloc;
};

module.exports = buildTable;
