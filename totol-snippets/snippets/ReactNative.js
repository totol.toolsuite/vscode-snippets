const rnlist = {
  headerCol1: "Nom",
  headerCol2: "Raccourcis",
  rows: [
    ["React Native Functional Component", "rnfc | imp"],
    ["&ltView&gt&lt/View&gt", "&ltVi | View"],
    ["&ltText&gt&lt/Text&gt", "&ltTe | Text"],
  ],
};

module.exports = rnlist;
