# Changelog

## Versions

### 0.0.1, 0.0.2, 0.0.3
- Add snippets for javascript and react native

### 0.0.4
- Add icon

### 0.0.5
- Configure react native type as javascript

### 0.0.6
- Configure command to get list of snippets

### 0.0.10 
- Update Doc and add js array funcs